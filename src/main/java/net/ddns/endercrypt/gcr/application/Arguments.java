package net.ddns.endercrypt.gcr.application;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.kohsuke.args4j.Argument;
import org.kohsuke.args4j.Option;

public class Arguments
{
	@Option(name = "-port", aliases = { "-p" }, required = false, usage = "port to open web interface in")
	private int port = 0;

	public int getPort()
	{
		return port;
	}

	@Argument
	private List<String> arguments = new ArrayList<String>();

	public List<String> getArguments()
	{
		return Collections.unmodifiableList(arguments);
	}
}
