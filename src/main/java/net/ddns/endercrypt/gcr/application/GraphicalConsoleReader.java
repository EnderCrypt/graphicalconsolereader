package net.ddns.endercrypt.gcr.application;


import java.awt.Desktop;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import net.ddns.endercrypt.gcr.data.Manager;
import net.ddns.endercrypt.gcr.web.mapper.NotFoundExceptionMapper;
import net.ddns.endercrypt.gcr.web.mapper.RuntimeExceptionMapper;


public class GraphicalConsoleReader
{
	public static void main(String[] args) throws URISyntaxException, IOException
	{
		// web server
		int port = randomPort();
		URI baseUri = new URI("http://0.0.0.0:" + port + "/");
		System.out.println("Hosting web view on: " + baseUri);
		ResourceConfig resourceConfig = new ResourceConfig().packages("net.ddns.endercrypt.gcr.web.rest");
		resourceConfig.register(new NotFoundExceptionMapper());
		resourceConfig.register(new RuntimeExceptionMapper());
		HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(baseUri, resourceConfig, false);
		httpServer.start();
		
		// open
		if (Desktop.isDesktopSupported())
		{
			Desktop.getDesktop().browse(new URI("http://localhost:" + port + "/"));
		}
		
		// convert array to list
		List<String> arguments = new ArrayList<String>();
		for (String arg : args)
		{
			arguments.add(arg);
		}
		if (arguments.size() < 1)
		{
			System.err.println("no program specified");
			System.exit(1);
		}
		
		// extract arguments
		Path path = Paths.get(arguments.remove(0));
		String argString = null;
		if (arguments.size() > 0)
		{
			argString = arguments.stream().collect(Collectors.joining(" "));
		}
		System.out.println("executing program " + path + (argString == null ? "" : " with arguments: " + argString));
		
		// start process
		Manager.execute(path, argString);
	}
	
	private static int randomPort()
	{
		try (ServerSocket ss = new ServerSocket(0))
		{
			return ss.getLocalPort();
		}
		catch (Exception e)
		{
			throw new RuntimeException("Unable to find a random port");
		}
	}
}
