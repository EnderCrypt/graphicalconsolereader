package net.ddns.endercrypt.gcr.web;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;

public class RestAssist
{
	private static final Path SERVE_DIRECTORY = Paths.get("static/");

	public static Response serveStatic(String file)
	{
		return serveStatic(Paths.get(file));
	}

	public static Response serveStatic(Path file)
	{
		Path path = SERVE_DIRECTORY.resolve(file);

		if (path.toString().contains(".."))
		{
			return Response.status(Status.FORBIDDEN).build();
		}

		if (Files.exists(path) == false)
		{
			return Response.status(404).build();
		}

		String content = null;
		try (InputStream input = new FileInputStream(path.toFile()))
		{
			content = IOUtils.toString(input, Charset.defaultCharset());
		}
		catch (IOException e)
		{
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(ExceptionUtils.getStackTrace(e)).build();
		}

		return Response.ok(content).build();
	}

	public static Response serveStatus(Status status)
	{
		return serveStatus(status, null);
	}

	public static Response serveStatus(Status status, String info)
	{
		// serve custom status page
		Path path = SERVE_DIRECTORY.resolve("status/" + status.getStatusCode() + ".html");
		if (Files.exists(path))
		{
			return loadStatusPage(path, status, info);
		}

		// serve default status page
		path = SERVE_DIRECTORY.resolve("status/default.html");
		if (Files.exists(path))
		{
			return loadStatusPage(path, status, info);
		}

		// serve basic status page
		StringBuilder sb = new StringBuilder();
		sb.append(status.getStatusCode());
		sb.append(": ");
		sb.append(status.getReasonPhrase());
		if (info != null)
		{
			sb.append("<br>");
			sb.append(info);
		}
		return Response.status(status).entity(sb.toString()).build();
	}

	private static Response loadStatusPage(Path path, Status status, String info)
	{
		String content = null;
		try (InputStream input = new FileInputStream(path.toFile()))
		{
			content = IOUtils.toString(input, Charset.defaultCharset());
		}
		catch (IOException e)
		{
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(ExceptionUtils.getStackTrace(e)).build();
		}

		Map<String, String> variables = new HashMap<>();
		variables.put("status-code", String.valueOf(status.getStatusCode()));
		variables.put("status-desc", status.getReasonPhrase());
		variables.put("status-info", info);

		content = setMetaVariable(content, variables);

		return Response.status(status).entity(content).build();
	}

	private static String setMetaVariable(String content, Map<String, String> variables)
	{
		for (Entry<String, String> entry : variables.entrySet())
		{
			String name = entry.getKey();
			String value = entry.getValue();

			if (name == null || value == null)
			{
				continue;
			}

			Pattern pattern = Pattern.compile("(<meta.+name=\"" + Pattern.quote(name) + "\".+content=\")(.*)(\">)");
			Matcher matcher = pattern.matcher(content);
			content = matcher.replaceAll("$1" + Matcher.quoteReplacement(value) + "$3");
		}

		return content;
	}
}
