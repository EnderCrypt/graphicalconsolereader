package net.ddns.endercrypt.gcr.web.mapper;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import net.ddns.endercrypt.gcr.web.RestAssist;

public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException>
{
	@Override
	public Response toResponse(NotFoundException e)
	{
		return RestAssist.serveStatus(Status.NOT_FOUND);
	}
}
