package net.ddns.endercrypt.gcr.web.mapper;

import javax.ws.rs.InternalServerErrorException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

import org.apache.commons.lang.exception.ExceptionUtils;

import net.ddns.endercrypt.gcr.web.RestAssist;

public class RuntimeExceptionMapper implements ExceptionMapper<Throwable>
{
	@Override
	public Response toResponse(Throwable e)
	{
		if (e instanceof InternalServerErrorException)
		{
			e = e.getCause();
		}
		e.printStackTrace();
		return RestAssist.serveStatus(Status.INTERNAL_SERVER_ERROR,
				ExceptionUtils.getStackTrace(e).replace("\n", "<br>"));
	}
}
