package net.ddns.endercrypt.gcr.web.rest;

import java.net.URI;
import java.net.URISyntaxException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class Root
{
	private static final URI home;
	static
	{
		URI tempHome = null;
		try
		{
			tempHome = new URI("/home");
		}
		catch (URISyntaxException e)
		{
			e.printStackTrace();
			System.exit(0);
		}

		home = tempHome;
	}

	@GET
	@Path("/")
	public Response index()
	{
		return Response.temporaryRedirect(home).build();
	}
}
