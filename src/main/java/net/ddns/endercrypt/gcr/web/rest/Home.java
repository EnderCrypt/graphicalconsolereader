package net.ddns.endercrypt.gcr.web.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import net.ddns.endercrypt.gcr.web.RestAssist;

@Path("/home")
public class Home
{
	@GET
	@Path("/")
	public Response index()
	{
		return RestAssist.serveStatic("html/index.html");
	}
}
