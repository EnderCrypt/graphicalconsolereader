package net.ddns.endercrypt.gcr.web.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import net.ddns.endercrypt.gcr.data.ApplicationExecutor;
import net.ddns.endercrypt.gcr.data.LogLine;
import net.ddns.endercrypt.gcr.data.Manager;

@Path("/api")
public class Api
{
	private static final Gson gson;
	static
	{
		GsonBuilder builder = new GsonBuilder();
		builder.setPrettyPrinting();
		gson = builder.create();
	}

	@GET
	@Path("/info")
	public Response info()
	{
		ApplicationExecutor executor = Manager.getExecutor();

		JsonObject root = new JsonObject();

		root.addProperty("logCount", Manager.logCount());
		root.addProperty("alive", executor.isRunning());
		root.addProperty("file", executor.getPath().toString());
		if (executor.isRunning() == false)
		{
			root.addProperty("exit", executor.getExitCode());
		}

		return Response.ok(gson.toJson(root)).build();
	}

	@GET
	@Path("/since/{since}")
	public Response since(@PathParam("since") int since)
	{
		JsonArray root = new JsonArray();
		List<LogLine> list = Manager.since(since);

		for (LogLine logLine : list)
		{
			root.add(logLine.toJson());
		}

		return Response.ok(gson.toJson(root)).build();
	}

	@POST
	@Path("/kill")
	public Response done()
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				Manager.getExecutor().getProcess().destroy();
				try
				{
					Thread.sleep(1000);
				}
				catch (InterruptedException e)
				{
					// ignore
				}
				System.out.println("Exited gracefully by kill signal from web interface");
				System.exit(1);
			}
		}).start();

		return Response.ok().build();
	}
}
