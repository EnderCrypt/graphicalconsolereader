package net.ddns.endercrypt.gcr.web.rest;

import java.util.List;
import java.util.stream.Collectors;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.Response;

import net.ddns.endercrypt.gcr.web.RestAssist;

@Path("/static")
public class Static
{
	@GET
	@Path("{any: .*}")
	public Response index(@PathParam("any") List<PathSegment> segments)
	{
		String file = segments.stream().map(p -> p.getPath()).collect(Collectors.joining("/"));

		return RestAssist.serveStatic(file);
	}
}
