package net.ddns.endercrypt.gcr.data;

import com.google.gson.JsonObject;

public class LogLine
{
	private final int lineNumber;
	private final String message;
	private final StdType stdType;

	public LogLine(int lineNumber, StdType stdType, String message)
	{
		this.lineNumber = lineNumber;
		this.message = message;
		this.stdType = stdType;
	}

	public int getLineNumber()
	{
		return lineNumber;
	}

	public StdType getStdType()
	{
		return stdType;
	}

	public String getMessage()
	{
		return message;
	}

	public JsonObject toJson()
	{
		JsonObject root = new JsonObject();

		root.addProperty("line", getLineNumber());
		root.addProperty("message", getMessage());
		root.addProperty("type", getStdType().toString());

		return root;
	}
}
