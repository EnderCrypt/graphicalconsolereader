package net.ddns.endercrypt.gcr.data;


import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;


public class ApplicationExecutor
{
	private Path path;
	private String arguments;
	private Process process;
	
	private InputStreamListener inputStream;
	private Thread inputStreamThread;
	
	private InputStreamListener errorStream;
	private Thread errorStreamThread;
	
	public ApplicationExecutor(Path path)
	{
		this(path, null);
	}
	
	public ApplicationExecutor(Path path, String arguments)
	{
		this.path = path;
		this.arguments = arguments;
	}
	
	public void start() throws IOException
	{
		process = Runtime.getRuntime().exec(path.toString() + (arguments == null ? "" : " " + arguments));
		
		inputStream = new InputStreamListener(process.getInputStream());
		inputStream.setMessageCallback(this::onOutput);
		inputStream.setExitCallback(this::onOutputExit);
		inputStreamThread = new Thread(inputStream);
		inputStreamThread.start();
		
		errorStream = new InputStreamListener(process.getErrorStream());
		errorStream.setMessageCallback(this::onError);
		errorStream.setExitCallback(this::onErrorExit);
		errorStreamThread = new Thread(errorStream);
		errorStreamThread.start();
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				try
				{
					process.waitFor();
					onExit(process);
				}
				catch (InterruptedException e)
				{
					e.printStackTrace();
					System.exit(1);
					return;
				}
			}
		}).start();
	}
	
	private void onExit(Process process) throws InterruptedException
	{
		Manager.addLog(StdType.SPECIAL, "[APPLICATION TERMINATED] Exit code: " + process.exitValue());
		Thread.sleep(TimeUnit.SECONDS.toMillis(10));
		System.exit(0);
	}
	
	private void onOutput(String line)
	{
		Manager.addLog(StdType.OUTPUT, line);
	}
	
	private void onOutputExit()
	{
		Manager.addLog(StdType.SPECIAL, "[OUTPUT STREAM EXITED]");
	}
	
	private void onError(String line)
	{
		Manager.addLog(StdType.ERROR, line);
	}
	
	private void onErrorExit()
	{
		Manager.addLog(StdType.SPECIAL, "[ERROR STREAM EXITED]");
	}
	
	public Process getProcess()
	{
		return process;
	}
	
	public Path getPath()
	{
		return path;
	}
	
	public boolean isRunning()
	{
		if (process == null)
		{
			return false;
		}
		if (process.isAlive() == false)
		{
			return false;
		}
		return true;
	}
	
	public int getExitCode()
	{
		return process.exitValue();
	}
}
