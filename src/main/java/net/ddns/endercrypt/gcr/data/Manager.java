package net.ddns.endercrypt.gcr.data;


import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;


public class Manager
{
	private static List<LogLine> lines = new ArrayList<>();
	
	public static void addLog(StdType stdType, String message)
	{
		// System.out.println("# " + message);
		lines.add(new LogLine(lines.size(), stdType, message));
	}
	
	public static int logCount()
	{
		return lines.size();
	}
	
	public static List<LogLine> since(int message)
	{
		List<LogLine> result = new ArrayList<>();
		for (int i = message; i < lines.size(); i++)
		{
			result.add(lines.get(i));
		}
		return result;
	}
	
	private static ApplicationExecutor executor;
	
	public static void execute(Path program, String arguments) throws IOException
	{
		if (executor != null)
		{
			throw new IllegalStateException("execution already started");
		}
		executor = new ApplicationExecutor(program, arguments);
		executor.start();
		
		/*
		 * executor.getProcess().onExit().thenRun(new Runnable() {
		 * 
		 * @Override public void run() { final int timeoutSeconds = 10; try {
		 * Thread.sleep(TimeUnit.SECONDS.toMillis(timeoutSeconds)); } catch
		 * (InterruptedException e) { // ignore }
		 * System.out.println("Timeout! exited after " + timeoutSeconds +
		 * " seconds"); System.exit(2); } });
		 */
	}
	
	public static ApplicationExecutor getExecutor()
	{
		return executor;
	}
}
