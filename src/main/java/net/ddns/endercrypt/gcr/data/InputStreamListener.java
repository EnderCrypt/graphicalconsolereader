package net.ddns.endercrypt.gcr.data;

import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.function.Consumer;

public class InputStreamListener implements Runnable
{
	private InputStream inputStream;
	private Consumer<String> messageCallback = null;
	private Runnable exitCallback = null;

	public InputStreamListener(InputStream inputStream)
	{
		this.inputStream = inputStream;
	}

	public synchronized void setMessageCallback(Consumer<String> messageCallback)
	{
		Objects.requireNonNull(messageCallback);
		this.messageCallback = messageCallback;
	}

	public synchronized void setExitCallback(Runnable exitCallback)
	{
		Objects.requireNonNull(exitCallback);
		this.exitCallback = exitCallback;
	}

	@Override
	public void run()
	{
		if (messageCallback == null)
		{
			throw new IllegalStateException(
					"cant start " + InputStreamListener.class.getSimpleName() + " whitout a set callback");
		}

		try
		{
			while (true)
			{
				messageCallback.accept(readLine());
			}
		}
		catch (EOFException e)
		{
			if (exitCallback != null)
			{
				exitCallback.run();
			}
			return;
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	private String readLine() throws IOException
	{
		StringBuilder sb = new StringBuilder();
		while (true)
		{
			int raw = inputStream.read();
			if (raw == -1)
			{
				if (sb.length() == 0)
				{
					throw new EOFException();
				}
				else
				{
					return sb.toString();
				}
			}
			char c = (char) raw;
			if (c == '\n')
			{
				return sb.toString();
			}
			sb.append(c);
		}
	}
}
