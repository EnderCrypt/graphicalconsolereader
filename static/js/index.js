var logs = [];
var logCount = 0;

var alive = true;

var jqInfoField;
var jqRegexInput;
var jqLog;

var intervalInfo;
var intervalLog;

$(function()
{
	jqInfoField		= $("#infoField");
	jqRegexInput	= $("#regexInput");
	jqLog			= $("#log");
	
	$.ajax({
		url: "/api/info",
		dataType: "json",
		success: function(info)
		{
			showInfo(info);
			
			intervalInfo	= setInterval(requestInfo, 100);
			intervalLog		= setTimeout(requestLog, 100);
		}
	});

});

function showInfo(info)
{
	var status = (info.alive) ? "Running" : "Exit: "+info.exit;
	
	var text = "File: "+info.file+" ["+status+"]<br>" +
			"Logs: "+logCount;
	jqInfoField.html(text);
}

function requestInfo()
{
	$.ajax({
		url: "/api/info",
		dataType: "json",
		success: function(info)
		{
			showInfo(info);
		}
	});
}

function killAll()
{
	if (alive == false)
	{
		return;
	}
	alive = false;
	
	var overlay = $("<div>");
	overlay.attr("id", "overlay");
	$("body").append(overlay);
	
	clearTimeout(intervalInfo);
	clearTimeout(intervalLog);

	console.log("Quitting...");
	$.post("/api/kill", function(data)
	{
		window.close(); // not always permitted
	});
}

function requestLog()
{
	$.ajax({
		url: "/api/since/"+logs.length,
		dataType: "json",
		success: function(newLogs)
		{
			intervalLog		= setTimeout(requestLog, 100);
			
			if (newLogs.length == 0)
			{
				return;
			}
			console.log("new logs:", newLogs);
			for(var i=0;i<newLogs.length;i++)
			{
				var newLog = newLogs[i];
				newLog.line = Number(newLog.line);
				
				logs[newLog.line] = newLog;
				htmlNewLog(newLog);
			}
			console.log("saved");
		}
	});
}

function htmlNewLog(log)
{
	var messageElement = $("<div>");
	messageElement.html(log.message);
	
	var root = $("<div>");
	root.addClass("logElement");
	root.addClass("std_"+log.type);
	root.append(messageElement);
	
	jqLog.append(root);
	
	if ((log.type == "OUTPUT") || (log.type == "ERROR"))
	{
		logCount += 1;
	}
}